package nl.hvz.backend.repositories;

import nl.hvz.backend.models.entities.Squad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SquadRepository extends JpaRepository<Squad, Long> {
    @Query("select (count(s) > 0) from Squad s where s.game.id = ?1 and s.id = ?2")
    boolean existsByGameIdAndId (Long gameId, Long squadId);

    @Query("select s from Squad s where s.game.id = ?1")
    List<Squad> findByGameId (Long gameId);
}
