package nl.hvz.backend.repositories;

import nl.hvz.backend.models.entities.SquadCheckin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SquadCheckinRepository extends JpaRepository<SquadCheckin, Long> {
    @Query("select s from SquadCheckin s where s.squadMember.squad.id = ?1 and s.squadMember.player.game.id = ?2")
    List<SquadCheckin> findBySquadMemberSquadIdAndSquadMemberPlayerGameId(Long squadId, Long gameId);
}
