package nl.hvz.backend.repositories;

import nl.hvz.backend.models.entities.Mission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MissionRepository extends JpaRepository<Mission, Long> {
    @Query("select m from Mission m where m.game.id = ?1")
    List<Mission> findByGameId (Long gameId);
}
