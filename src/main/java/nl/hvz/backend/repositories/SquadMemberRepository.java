package nl.hvz.backend.repositories;

import nl.hvz.backend.models.entities.SquadMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SquadMemberRepository extends JpaRepository<SquadMember, Long> {
    @Query("select count(s) from SquadMember s where s.squad.id = ?1")
    long countBySquadId(Long squadId);

    @Query("select count(s) from SquadMember s where s.squad.id = ?1 and s.player.isHuman = ?2")
    long countBySquadIdAndPlayerIsHuman(Long squadId, boolean isHuman);

    @Query("select s from SquadMember s where s.player.id = ?1")
    Optional<SquadMember> findByPlayerId(Long playerId);

}
