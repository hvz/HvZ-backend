package nl.hvz.backend.repositories;

import nl.hvz.backend.models.entities.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
    @Query("select count(p) from Player p where p.game.id = ?1")
    long countByGameId(Long gameId);

    @Query("select p.biteCode from Player p where p.game.id = ?1")
    Set<String> findBiteCodeByGameId(Long gameId);

    @Query("select p from Player p where p.user.id = ?1 and p.game.id = ?2")
    Optional<Player> findByUserIdAndGameId(String userId, Long gameId);

    @Query("select p from Player p where p.biteCode = ?1 and p.game.id = ?2")
    Optional<Player> findByBiteCodeAndGameId(String biteCode, Long gameId);

    @Query("select p from Player p where p.game.id = ?1")
    List<Player> findByGameId (Long gameId);

    @Modifying
    @Query("update Player p set p.isHuman = false where p.isPatientZero = true")
    void turnPatientZeroesToZombies();
}
