package nl.hvz.backend.repositories;

import nl.hvz.backend.models.entities.ChatMessage;
import nl.hvz.backend.models.entities.Squad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatRepository extends JpaRepository<ChatMessage, Long> {
    @Query("select c from ChatMessage c where c.game.id = ?1 and c.squad.id = ?2")
    List<ChatMessage> findByGameIdAndSquadId(Long gameId, Long squadId);
    @Query("select c from ChatMessage c where c.game.id = ?1")
    List<ChatMessage> findByGameId(Long gameId);
}
