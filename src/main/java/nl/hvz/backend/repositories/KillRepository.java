package nl.hvz.backend.repositories;

import nl.hvz.backend.models.entities.Kill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KillRepository extends JpaRepository<Kill, Long> {
    @Query("select k from Kill k where k.killer.game.id = ?1")
    List<Kill> findByKillerGameId(Long gameId);
}
