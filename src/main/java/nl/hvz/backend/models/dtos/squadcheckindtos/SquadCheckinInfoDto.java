package nl.hvz.backend.models.dtos.squadcheckindtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Squad checkin data to be displayed
 */
@Data
public class SquadCheckinInfoDto implements Serializable {
    private Long id;
    private String timestamp;
    private LocationDto location;
    private Long squadMemberId;
    private Long playerId;
    private String username;
}
