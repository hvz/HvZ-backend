package nl.hvz.backend.models.dtos.missiondtos;

import lombok.Data;
import nl.hvz.backend.models.Faction;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Data to be displayed when a specific mission has been displayed.
 */
@Data
public class MissionDetailsDto implements Serializable {
    private Long id;
    private String title;
    private String faction;
    private String createdAt;
    private String description;
    private LocationDto marker;
}
