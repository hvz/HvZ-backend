package nl.hvz.backend.models.dtos.chatmessagedto;

import lombok.Data;
import nl.hvz.backend.models.Faction;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Data that every chat message contains.
 */
@Data
public class ChatMessageInfoDto implements Serializable {
    private Long id;
    private String message;
    private String faction;
    private String timestamp;
    private String username;
    private Boolean isAdmin = false;
}
