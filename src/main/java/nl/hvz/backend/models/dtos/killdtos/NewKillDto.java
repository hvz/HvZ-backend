package nl.hvz.backend.models.dtos.killdtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

/**
 * Data to be transferred when a new kill needs to be instantiated.
 * The bite code will be used if the client is not an admin.
 * Otherwise, the killer and victim integers will be used.
 */
@Data
public class NewKillDto {
    private String biteCode;
    private Long killerId;
    private Long victimId;
    private LocationDto location;
}
