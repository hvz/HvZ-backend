package nl.hvz.backend.models.dtos.killdtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Details to be displayed when a specific kill is requested.
 */
@Data
public class KillDetailsDto implements Serializable {
    private Long id;
    private String timeOfDeath;
    private LocationDto location;
    private Long killerId;
    private String killerUsername;
    private Long victimId;
    private String victimUsername;
}
