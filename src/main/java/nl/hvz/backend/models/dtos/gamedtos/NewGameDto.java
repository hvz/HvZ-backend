package nl.hvz.backend.models.dtos.gamedtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;

/**
 * Data to be transferred when creating a new game.
 */
@Data
public class NewGameDto implements Serializable {
    private String name;
    private String description;
    private Double radius;
    private LocationDto center;
}
