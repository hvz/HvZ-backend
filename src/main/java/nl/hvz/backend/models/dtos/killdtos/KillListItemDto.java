package nl.hvz.backend.models.dtos.killdtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Data of kills to be displayed in kill lists.
 */
@Data
public class KillListItemDto implements Serializable {
    private Long id;
    private String timeOfDeath;
    private LocationDto location;
}
