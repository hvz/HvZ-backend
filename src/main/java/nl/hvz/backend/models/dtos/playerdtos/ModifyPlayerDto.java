package nl.hvz.backend.models.dtos.playerdtos;

import lombok.Data;

import java.io.Serializable;

/**
 * Player data to be transferred to modify the status of a player (admin only).
 */
@Data
public class ModifyPlayerDto implements Serializable {
    private Boolean isHuman = true;
    private Boolean isPatientZero = false;
}
