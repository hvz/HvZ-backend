package nl.hvz.backend.models.dtos.squaddtos;

import lombok.Data;

import java.io.Serializable;

/**
 * Data to be transferred when modifying an existing squad.
 */
@Data
public class ModifySquadDto implements Serializable {
    private String name;
}
