package nl.hvz.backend.models.dtos.squaddtos;

import lombok.Data;

import java.io.Serializable;

/**
 * Data to be transferred when adding a new squad
 */
@Data
public class NewSquadDto implements Serializable {
    private String name;
}
