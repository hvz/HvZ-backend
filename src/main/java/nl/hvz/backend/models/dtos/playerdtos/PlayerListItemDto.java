package nl.hvz.backend.models.dtos.playerdtos;

import lombok.Data;

import java.io.Serializable;

/**
 * Non-sensitive player data that all logged in players are allowed to see. Used when a list of players is requested.
 */
@Data
public class PlayerListItemDto implements Serializable {
    private Long id;
    private Boolean isHuman = true;
    private String username;
}
