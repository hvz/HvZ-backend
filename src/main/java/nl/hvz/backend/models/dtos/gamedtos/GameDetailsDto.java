package nl.hvz.backend.models.dtos.gamedtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;

/**
 * Information about the game to be displayed when selecting a game to see details after login.
 */
@Data
public class GameDetailsDto implements Serializable {
    private Long id;
    private String name;
    private String gameState;
    private Long numberOfPlayers;
    private String published;
    private String description;
    private Double radius;
    private LocationDto center;
}