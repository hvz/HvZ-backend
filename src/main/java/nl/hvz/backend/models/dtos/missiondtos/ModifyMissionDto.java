package nl.hvz.backend.models.dtos.missiondtos;

import lombok.Data;
import nl.hvz.backend.models.Faction;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;

/**
 * Data to be transferred when modifying an existing mission.
 */
@Data
public class ModifyMissionDto implements Serializable {
    private String title;
    private String faction;
    private String description;
    private LocationDto marker;
}
