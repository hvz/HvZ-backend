package nl.hvz.backend.models.dtos.squaddtos;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Squad member data to displayed when a list of squad members has been requested.
 */
@Data
public class SquadMemberDto implements Serializable {
    private String joinedAt;
    private Long playerId;
    private Boolean isHuman = true;
    private String username;
}
