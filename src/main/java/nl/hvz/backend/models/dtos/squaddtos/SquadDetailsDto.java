package nl.hvz.backend.models.dtos.squaddtos;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Squad data to be displayed when a specific squad has been requested.
 */
@Data
public class SquadDetailsDto implements Serializable {
    private Long id;
    private String name;
    private List<SquadMemberDto> squadMembers = new ArrayList<>();
}
