package nl.hvz.backend.models.dtos.killdtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Data to be transferred when a kill record needs to be modified.
 * The killer can only change the location.
 */
@Data
public class ModifyKillDto implements Serializable {
    private LocationDto location;
    private Long killerId;
    private Long victimId;
}
