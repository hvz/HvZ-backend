package nl.hvz.backend.models.dtos;

import lombok.Data;

import java.io.Serializable;

/**
 * Location data, both for display and modification.
 */
@Data
public class LocationDto implements Serializable {
    private Double longitude;
    private Double latitude;
}
