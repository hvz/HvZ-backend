package nl.hvz.backend.models.dtos.squaddtos;

import lombok.Data;

import java.io.Serializable;

/**
 * Squad data to be displayed when a list of squads has been requested
 */
@Data
public class SquadListItemDto implements Serializable {
    private Long id;
    private String name;
    private Long numberOfMembers;
    private Long numberOfDeceased;
}
