package nl.hvz.backend.models.dtos.gamedtos;

import lombok.Data;
import nl.hvz.backend.models.GameState;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;

/**
 * Data to be transferred when updating the game.
 */
@Data
public class ModifyGameDto implements Serializable {
    private String name;
    private String description;
    private String gameState;
    private Double radius;
    private LocationDto center;
}
