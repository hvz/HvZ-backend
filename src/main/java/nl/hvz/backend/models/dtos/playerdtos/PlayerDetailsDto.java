package nl.hvz.backend.models.dtos.playerdtos;

import lombok.Data;

import java.io.Serializable;

/**
 * Specific layer data that only the admin and the current player is allowed to see.
 */
@Data
public class PlayerDetailsDto implements Serializable {
    private Long id;
    private Boolean isHuman = true;
    private Boolean isPatientZero = false;
    private String biteCode;
    private String username;
}
