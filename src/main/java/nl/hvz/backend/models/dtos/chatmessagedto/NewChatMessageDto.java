package nl.hvz.backend.models.dtos.chatmessagedto;

import lombok.Data;
import nl.hvz.backend.models.Faction;

import java.io.Serializable;

/**
 * Data to be transferred when sending a new chat message.
 */
@Data
public class NewChatMessageDto implements Serializable {
    private String message;
    private String faction;
}
