package nl.hvz.backend.models.dtos.missiondtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Data to be displayed when a list of missions is requested.
 */
@Data
public class MissionListItemDto implements Serializable {
    private Long id;
    private String title;
    private String createdAt;
    private LocationDto marker;
}
