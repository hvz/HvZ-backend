package nl.hvz.backend.models.dtos.missiondtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;

/**
 * Data to be transferred when creating a new mission.
 */
@Data
public class NewMissionDto implements Serializable {
    private String title;
    private String faction;
    private String description;
    private LocationDto marker;
}
