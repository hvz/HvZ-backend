package nl.hvz.backend.models.dtos.squadcheckindtos;

import lombok.Data;
import nl.hvz.backend.models.dtos.LocationDto;

import java.io.Serializable;

/**
 * Data to be transferred when adding a new checkin.
 */
@Data
public class NewSquadCheckinDto implements Serializable {
    private LocationDto location;
}
