package nl.hvz.backend.models.dtos.gamedtos;

import lombok.Data;

import java.io.Serializable;

/**
 * Game information to be displayed in a list of games publicly available.
 */
@Data
public class GameListItemDto implements Serializable {
    private Long id;
    private String name;
    private String gameState;
    private Long numberOfPlayers;
    private String published;
}
