package nl.hvz.backend.models;

public enum GameState {
    REGISTRATION,
    IN_PROGRESS,
    COMPLETED
}
