package nl.hvz.backend.models.entities;

import lombok.Getter;
import lombok.Setter;
import nl.hvz.backend.models.GameState;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@javax.persistence.Entity
@Getter
@Setter
@Table(name = "game")
public class Game implements Entity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title", length = 40, nullable = false)
    private String name;

    @Column(name = "description", length = 200)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "game_state", nullable = false)
    private GameState gameState;

    @CreationTimestamp
    @Column(name = "published", nullable = false)
    private ZonedDateTime published;

    @Column(name = "radius")
    private Double radius;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "center")
    private Location center;

    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    @OrderBy("timestamp")
    private List<ChatMessage> chatMessages = new java.util.ArrayList<>();

    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    @OrderBy
    private List<Squad> squads = new java.util.ArrayList<>();

    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    @OrderBy("createdAt DESC")
    private List<Mission> missions = new java.util.ArrayList<>();

    @OneToMany(mappedBy = "game", cascade = {CascadeType.REMOVE})
    @OrderBy
    private List<Player> players = new java.util.ArrayList<>();
}
