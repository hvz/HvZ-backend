package nl.hvz.backend.models.entities;

/**
 * Abstraction of all database entities.
 * @param <ID> The data type of the id.
 */
public interface Entity<ID> {
    ID getId();
    void setId(ID id);
}
