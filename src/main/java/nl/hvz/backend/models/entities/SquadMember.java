package nl.hvz.backend.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@javax.persistence.Entity
@Getter
@Setter
@Table(name = "squad_member")
public class SquadMember implements Entity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @CreationTimestamp
    @Column(name = "joined_at", nullable = false)
    private ZonedDateTime joinedAt;

    @OneToOne(optional = false)
    @JoinColumn(name = "player", nullable = false, unique = true)
    private Player player;

    @ManyToOne(optional = false)
    @JoinColumn(name = "squad", nullable = false)
    private Squad squad;

    @OneToMany(mappedBy = "squadMember", cascade = CascadeType.REMOVE)
    @OrderBy("timestamp DESC")
    private List<SquadCheckin> squadCheckins = new java.util.ArrayList<>();
}
