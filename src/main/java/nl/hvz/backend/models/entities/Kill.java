package nl.hvz.backend.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;

@javax.persistence.Entity
@Getter
@Setter
@Table(name = "kill")
public class Kill implements Entity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @CreationTimestamp
    @Column(name = "time_of_death", nullable = false)
    private ZonedDateTime timeOfDeath;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "location")
    private Location location;

    @ManyToOne(optional = false)
    @JoinColumn(name = "killer", nullable = false)
    private Player killer;

    @OneToOne(optional = false)
    @JoinColumn(name = "victim", nullable = false, unique = true)
    private Player victim;
}
