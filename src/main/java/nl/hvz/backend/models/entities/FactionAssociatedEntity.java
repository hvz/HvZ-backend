package nl.hvz.backend.models.entities;

import nl.hvz.backend.models.Faction;

/**
 * Entity with a faction property.
 */
public interface FactionAssociatedEntity<ID> extends Entity<ID> {
    Faction getFaction();
    void setFaction(Faction faction);
}
