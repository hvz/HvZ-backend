package nl.hvz.backend.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;

@javax.persistence.Entity
@Getter
@Setter
@Table(name = "squad_checkin")
public class SquadCheckin implements Entity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @CreationTimestamp
    @Column(name = "timestamp", nullable = false)
    private ZonedDateTime timestamp;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "location", nullable = false)
    private Location location;

    @ManyToOne(optional = false)
    @JoinColumn(name = "squad_member", nullable = false)
    private SquadMember squadMember;
}
