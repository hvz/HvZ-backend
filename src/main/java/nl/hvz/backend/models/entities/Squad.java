package nl.hvz.backend.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@javax.persistence.Entity
@Getter
@Setter
@Table(name = "squad")
public class Squad implements Entity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 40)
    private String name;

    @ManyToOne(optional = false)
    @JoinColumn(name = "game", nullable = false)
    private Game game;

    @OneToMany(mappedBy = "squad", cascade = CascadeType.REMOVE)
    @OrderBy("timestamp")
    private List<ChatMessage> chatMessages = new java.util.ArrayList<>();

    @OneToMany(mappedBy = "squad", cascade = CascadeType.REMOVE)
    @OrderBy("joinedAt DESC")
    private List<SquadMember> squadMembers = new java.util.ArrayList<>();
}
