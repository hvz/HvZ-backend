package nl.hvz.backend.models.entities;

import lombok.Getter;
import lombok.Setter;
import nl.hvz.backend.models.Faction;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;

@javax.persistence.Entity
@Getter
@Setter
@Table(name = "chat_message")
public class ChatMessage implements FactionAssociatedEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "message", nullable = false, length = 200)
    private String message;

    @Enumerated(EnumType.STRING)
    @Column(name = "faction", nullable = false)
    private Faction faction;

    @CreationTimestamp
    @Column(name = "timestamp", nullable = false)
    private ZonedDateTime timestamp;

    @ManyToOne(optional = false)
    @JoinColumn(name = "message_owner", nullable = false)
    private User messageOwner;

    @ManyToOne
    @JoinColumn(name = "squad")
    private Squad squad;

    @ManyToOne(optional = false)
    @JoinColumn(name = "game", nullable = false)
    private Game game;
}
