package nl.hvz.backend.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@javax.persistence.Entity
@Getter
@Setter
@Table(name = "player")
public class Player implements Entity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "is_human", nullable = false)
    private Boolean isHuman = true;

    @Column(name = "is_patient_zero", nullable = false)
    private Boolean isPatientZero = false;

    @Column(name = "bite_code", length = 4)
    private String biteCode;

    @ManyToOne(optional = false)
    @JoinColumn(name = "usr", nullable = false)
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "game", nullable = false)
    private Game game;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "player", cascade = CascadeType.REMOVE)
    private SquadMember squadMember;

    @OneToMany(mappedBy = "killer", cascade = CascadeType.REMOVE)
    @OrderBy("timeOfDeath DESC")
    private List<Kill> kills = new java.util.ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "victim", cascade = CascadeType.REMOVE)
    private Kill deathCause;
}
