package nl.hvz.backend.models.entities;

import lombok.Getter;
import lombok.Setter;
import nl.hvz.backend.models.Faction;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;

@javax.persistence.Entity
@Getter
@Setter
@Table(name = "mission")
public class Mission implements FactionAssociatedEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title", nullable = false, length = 40)
    private String title;

    @Enumerated(EnumType.STRING)
    @Column(name = "faction", nullable = false)
    private Faction faction;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    private ZonedDateTime createdAt;

    @Column(name = "description", length = 200)
    private String description;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "marker")
    private Location marker;

    @ManyToOne(optional = false)
    @JoinColumn(name = "game", nullable = false)
    private Game game;
}
