package nl.hvz.backend.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@javax.persistence.Entity
@Getter
@Setter
@Table(name = "users")
public class User implements Entity<String> {
    @Id
    @Column(name = "id", nullable = false, length = 50)
    private String id;

    @Column(name = "username", nullable = false, length = 25)
    private String username;

    @Column(name = "is_admin", nullable = false)
    private Boolean isAdmin = false;

    @OneToMany(mappedBy = "user")
    @OrderBy
    private List<Player> players = new java.util.ArrayList<>();

    @OneToMany(mappedBy = "messageOwner")
    @OrderBy("timestamp")
    private List<ChatMessage> chatMessages = new java.util.ArrayList<>();
}
