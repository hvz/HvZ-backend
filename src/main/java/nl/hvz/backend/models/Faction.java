package nl.hvz.backend.models;

public enum Faction {
    HUMAN,
    ZOMBIE,
    GLOBAL
}
