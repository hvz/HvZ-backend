package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.Mission;
import nl.hvz.backend.models.entities.User;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public interface MissionService extends CrudService<Mission,Long> {

    /**
     * @param gameId The game id.
     * @param user The user that wants to have a list of missions.
     * @return A list of missions in a game.
     */
    List<Mission> findAll(Long gameId, User user);

    /**
     * @param missionId The mission id.
     * @param gameId The game id.
     * @param user The player who wants to see the mission.
     * @return The mission object if the user is allowed to see it.
     * @throws ResponseStatusException If the mission cannot be found
     * @throws ResponseStatusException If the mission is not in the current game.
     * @throws ResponseStatusException If the player is not allowed to see the mission.
     */
    Mission findById(Long missionId, Long gameId, User user) throws ResponseStatusException;

    void deleteById(Long missionId, Long gameId, User user);
}
