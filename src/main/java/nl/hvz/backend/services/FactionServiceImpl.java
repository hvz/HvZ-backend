package nl.hvz.backend.services;

import lombok.AllArgsConstructor;
import nl.hvz.backend.models.Faction;
import nl.hvz.backend.models.entities.FactionAssociatedEntity;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FactionServiceImpl<T extends FactionAssociatedEntity<?>> implements FactionService<T> {
    private final PlayerService playerService;

    @Override
    public List<T> filterByFaction(List<T> input, Long gameId, User user) {
        if (user.getIsAdmin()) return input;
        Player player = playerService.findPlayerInGame(user.getId(), gameId);
        return input.stream().filter(m -> {
            if (m.getFaction().equals(Faction.GLOBAL)) return true;
            return player.getIsHuman() != m.getFaction().equals(Faction.ZOMBIE);
        }).toList();
    }
}
