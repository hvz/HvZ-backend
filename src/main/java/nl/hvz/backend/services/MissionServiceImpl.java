package nl.hvz.backend.services;

import nl.hvz.backend.models.Faction;
import nl.hvz.backend.models.entities.Mission;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.repositories.MissionRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class MissionServiceImpl extends CrudServiceImpl<Mission, Long, MissionRepository> implements MissionService {
    private final PlayerService playerService;
    private final FactionService<Mission> factionService;

    public MissionServiceImpl(MissionRepository repository, PlayerService playerService, FactionService<Mission> factionService) {
        super(repository);
        this.playerService = playerService;
        this.factionService = factionService;
    }

    @Override
    public List<Mission> findAll(Long gameId, User user) {
        List<Mission> missions = repository.findByGameId(gameId);
        return factionService.filterByFaction(missions, gameId, user);
    }

    @Override
    public Mission findById(Long missionId, Long gameId, User user) throws ResponseStatusException {
        Mission mission = super.findById(missionId);
        if (!mission.getGame().getId().equals(gameId))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Mission not in current game");
        if (!user.getIsAdmin()) {
            Player player = playerService.findPlayerInGame(user.getId(), gameId);
            if (player.getIsHuman() != mission.getFaction().equals(Faction.HUMAN))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                        "Not allowed to get human mission as zombie or vice versa.");
        }
        return mission;
    }

    @Override
    public void deleteById(Long missionId, Long gameId, User user) {
        this.findById(missionId, gameId, user);
        super.deleteById(missionId);
    }
}
