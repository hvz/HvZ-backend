package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.Squad;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.repositories.SquadRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class SquadServiceImpl extends CrudServiceImpl<Squad, Long, SquadRepository> implements SquadService {
    private final GameService gameService;
    private final PlayerService playerService;

    public SquadServiceImpl(SquadRepository repository, GameService gameService, PlayerService playerService) {
        super(repository);
        this.gameService = gameService;
        this.playerService = playerService;
    }

    @Override
    public Squad findById(Long squadId) throws ResponseStatusException {
        if (squadId == null) return null;
        return super.findById(squadId);
    }

    @Override
    public Squad findById(Long squadId, Long gameId, User user) throws ResponseStatusException {
        if (!squadInGame(gameId, squadId))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Squad is not in current game.");
        if (!user.getIsAdmin()) {
            playerService.findPlayerInGame(user.getId(), gameId).getId();
        }
        return super.findById(squadId);
    }

    public boolean squadInGame(Long gameId, Long squadId) throws ResponseStatusException{
        if (!existsById(squadId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Squad", squadId));
        }
        if (!gameService.existsById(gameId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Game", gameId));
        }
        return repository.existsByGameIdAndId(gameId, squadId);
    }

    @Override
    public List<Squad> findAll(Long gameId) throws ResponseStatusException {
        if (!gameService.existsById(gameId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Game", gameId));
        }
        return repository.findByGameId(gameId);
    }

    @Override
    public Squad create(Squad squad, User user) throws ResponseStatusException {
        Long gameId = squad.getGame().getId();
        if (!user.getIsAdmin()) {
            Player player = playerService.findPlayerInGame(user.getId(), gameId);
            if (!player.getIsHuman())
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Zombies are not allowed to create squads.");
        }
        return super.create(squad);
    }

    @Override
    public void deleteById(Long squadId, Long gameId) throws ResponseStatusException {
        if (!squadInGame(gameId, squadId))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Squad is not in current game.");
        super.deleteById(squadId);
    }
}
