package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.SquadCheckin;
import nl.hvz.backend.models.entities.SquadMember;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.repositories.SquadCheckinRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class SquadCheckinServiceImpl extends CrudServiceImpl<SquadCheckin, Long, SquadCheckinRepository>
        implements SquadCheckinService {
    private final SquadService squadService;
    private final PlayerService playerService;
    private final SquadMemberService squadMemberService;

    public SquadCheckinServiceImpl(SquadCheckinRepository repository, SquadService squadService,
                                   PlayerService playerService, SquadMemberService squadMemberService) {
        super(repository);
        this.squadService = squadService;
        this.playerService = playerService;
        this.squadMemberService = squadMemberService;
    }

    @Override
    public List<SquadCheckin> findAll(Long gameId, Long squadId, User user) throws ResponseStatusException {
        if (!user.getIsAdmin()) {
            Player currentPlayer = playerService.findPlayerInGame(user.getId(), gameId);
            SquadMember squadMember = squadMemberService.getSquadMemberFromPlayerId(currentPlayer.getId());
            if (!squadMember.getSquad().getId().equals(squadId)) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Current player is not a member of this squad.");
            }
            if (!squadMember.getPlayer().getIsHuman()) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Current player is not human.");
            }
        }
        if (!squadService.squadInGame(gameId, squadId)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Squad is not in current game.");
        }
        return repository.findBySquadMemberSquadIdAndSquadMemberPlayerGameId(squadId, gameId);
    }

    @Override
    public SquadCheckin create(SquadCheckin entity, Long gameId) throws ResponseStatusException {
        if (!squadService.squadInGame(gameId, entity.getSquadMember().getSquad().getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Squad is not in current game.");
        if (!entity.getSquadMember().getPlayer().getIsHuman())
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Zombies not allowed to checkin.");
        return super.create(entity);
    }
}
