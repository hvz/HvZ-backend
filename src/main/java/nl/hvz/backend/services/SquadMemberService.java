package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.SquadMember;
import org.mapstruct.Named;
import org.springframework.web.server.ResponseStatusException;

public interface SquadMemberService extends CrudService<SquadMember,Long> {
    /**
     * @param squadId The id of a squad.
     * @return The number of members in the squad with the given id.
     * @throws ResponseStatusException If the squad with the given id does not exist.
     */
    @Named("numberOfSquadMembers")
    long numberOfSquadMembersInSquadWithId(Long squadId) throws ResponseStatusException;
    /**
     * @param squadId The id of a squad.
     * @return The number of deceased members in the squad with the given id (i.e. the number of zombies).
     * @throws ResponseStatusException If the squad with the given id does not exist.
     */
    @Named("numberOfDeceasedSquadMembers")
    long numberOfDeceasedSquadMembersInSquadWithId(Long squadId) throws ResponseStatusException;

    /**
     * @param playerId The id of the player
     * @return The squad member object
     * @throws ResponseStatusException If the player does not exist or if the player is not in a squad.
     */
    SquadMember getSquadMemberFromPlayerId(Long playerId) throws ResponseStatusException;

    /**
     * Checks if the player has not joined a squad yet.
     * @param playerId The player id.
     * @return {@code true} if the player is not in a squad, {@code false} otherwise.
     * @throws ResponseStatusException If the player with specified id does not exist.
     */
    boolean inSquad(Long playerId) throws ResponseStatusException;

    /**
     * @param squadMember The squad member to be added.
     * @param gameId The game in which the squad member should be added.
     * @return The squad member entity after it has been persisted.
     * @throws ResponseStatusException If the player is not in the game where the squad member is located.
     */
    SquadMember create(SquadMember squadMember, Long gameId) throws ResponseStatusException;

    /**
     * @param squadMemberId The id of the squad member to be deleted.
     * @param squadId The of the squad that the current player wants to leave.
     * @throws ResponseStatusException If the squad member is not in the game.
     */
    void deleteById(Long squadMemberId, Long squadId) throws ResponseStatusException;
}
