package nl.hvz.backend.services;

import nl.hvz.backend.models.GameState;
import nl.hvz.backend.models.entities.Game;
import nl.hvz.backend.repositories.GameRepository;
import nl.hvz.backend.repositories.PlayerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
public class GameServiceImpl extends CrudServiceImpl<Game, Long, GameRepository> implements GameService {

    private final PlayerRepository playerRepository;

    public GameServiceImpl(GameRepository repository, PlayerRepository playerRepository) {
        super(repository);
        this.playerRepository = playerRepository;
    }

    @Override
    public Game findById(Long aLong) throws ResponseStatusException {
        return super.findById(aLong);
    }

    @Override
    @Transactional
    public Game update(Game game) {
        if (game.getGameState().equals(GameState.IN_PROGRESS)) {
            playerRepository.turnPatientZeroesToZombies();
        }
        return super.update(game);
    }
}
