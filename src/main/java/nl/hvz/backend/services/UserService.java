package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.User;
import org.mapstruct.AfterMapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.server.ResponseStatusException;

/**
 * Service that is responsible for current user management.
 */
public interface UserService {
    /**
     * Get the username by checking the nickname claim.
     * @param jwt The jwt, must contain claim {@code "nickname"}.
     * @return The username.
     * @throws ResponseStatusException if the jwt does not contain a nickname claim.
     */
    @Named("toUsernameFromJwt")
    String getUsernameFromJwt(Jwt jwt) throws ResponseStatusException;

    /**
     * Get the admin role by checking if the jwt contains {@code "ADMIN"} in the roles claim.
     * @param jwt The jwt, must contain claim for roles
     * @return {@code true} if the admin role is present, {@code false} otherwise.
     * @throws ResponseStatusException if the jwt does not contain a claim for roles.
     */
    @Named("toIsAdminFromJwt")
    boolean getAdminRoleFromJwt(Jwt jwt) throws ResponseStatusException;

    /**
     * Persist a user entity.
     * @param user The user entity to be persisted.
     * @return The user entity after it has been persisted.
     */
    @AfterMapping
    User persist(@MappingTarget User user);
}
