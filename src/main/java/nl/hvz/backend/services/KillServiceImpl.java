package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.Kill;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.repositories.KillRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class KillServiceImpl extends CrudServiceImpl<Kill, Long, KillRepository> implements KillService {
    private final GameService gameService;
    private final PlayerService playerService;

    public KillServiceImpl(KillRepository repository, GameService gameService, PlayerService playerService) {
        super(repository);
        this.gameService = gameService;
        this.playerService = playerService;
    }

    @Override
    public List<Kill> findAll(Long gameId) throws ResponseStatusException {
        if (!gameService.existsById(gameId)) throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Game", gameId));
        return repository.findByKillerGameId(gameId);
    }

    @Override
    public Kill findById(Long killId, Long gameId) throws ResponseStatusException {
        Kill kill = super.findById(killId);
        if (!kill.getKiller().getGame().getId().equals(gameId))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested kill not in current game.");
        return kill;
    }

    @Override
    public void deleteById(Long killId, Long gameId) throws ResponseStatusException {
        this.findById(killId, gameId);
        super.deleteById(killId);
    }

    @Override
    public Kill update(Kill kill, Long gameId, User user) throws ResponseStatusException {
        if (!user.getIsAdmin()) {
            if (!kill.getKiller().getId().equals(playerService.findPlayerInGame(user.getId(), gameId).getId()))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Only the killer and admin may modify kills");
        }
        return super.update(kill);
    }

    @Override
    @Transactional
    public Kill create(Kill kill) {
        Kill newKill = super.create(kill);
        playerService.findById(kill.getVictim().getId()).setIsHuman(false);
        return newKill;
    }
}
