package nl.hvz.backend.services;

import lombok.AllArgsConstructor;
import nl.hvz.backend.models.entities.Entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.lang.reflect.ParameterizedType;
import java.util.List;

@AllArgsConstructor
public abstract class CrudServiceImpl<T extends Entity<ID>, ID, R extends JpaRepository<T,ID>>
        implements CrudService<T,ID> {
    protected final R repository;

    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    public T findById(ID id) throws ResponseStatusException {
        String entityType = ((Class<?>) (
                (ParameterizedType) repository.getClass().getInterfaces()[0].getGenericInterfaces()[0]
        ).getActualTypeArguments()[0]).getSimpleName();
        return repository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, entityType, id)));
    }

    @Override
    public boolean existsById(ID id) {
        return repository.existsById(id);
    }

    @Override
    public T update(T entity) throws ResponseStatusException {
        if (!existsById(entity.getId())) throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, entity.getClass().getTypeName(), entity.getId()));
        return repository.save(entity);
    }

    @Override
    public T create(T entity) throws ResponseStatusException {
        if (entity.getId() != null) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                String.format("Id of created %s should not be specified.", entity.getClass().getTypeName()));
        return repository.save(entity);
    }

    @Override
    public void deleteById(ID id) {
        repository.deleteById(id);
    }
}
