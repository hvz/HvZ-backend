package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.ChatMessage;
import nl.hvz.backend.models.entities.User;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public interface ChatService extends CrudService<ChatMessage,Long> {
    /**
     * @param gameId The game id.
     * @param squadId The squad id, {@code null} if chat messages are not associated with a squad.
     * @param user The user.
     * @return All chat messages filtered by game, squad and what the user is allowed to see.
     * @throws ResponseStatusException if the game does not exist or the squad is invalid.
     */
    List<ChatMessage> findAll(Long gameId, Long squadId, User user) throws ResponseStatusException;

    /**
     * @param chatMessage The chat message to be added.
     * @param gameId The game the chat message belongs to.
     * @param user The user who sends the message.
     * @return The chat message after it has been saved.
     * @throws ResponseStatusException if the user is not an admin or in the current game or squad.
     */
    ChatMessage create(ChatMessage chatMessage, Long gameId, User user) throws ResponseStatusException;
}
