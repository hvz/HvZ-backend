package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.Game;
import org.mapstruct.Named;
import org.springframework.web.server.ResponseStatusException;

public interface GameService extends CrudService<Game,Long> {
    @Override
    @Named("findGameById")
    Game findById(Long gameId) throws ResponseStatusException;
}
