package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.SquadMember;
import nl.hvz.backend.repositories.SquadMemberRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class SquadMemberServiceImpl extends CrudServiceImpl<SquadMember, Long, SquadMemberRepository>
        implements SquadMemberService {

    private final SquadService squadService;
    private final PlayerService playerService;

    public SquadMemberServiceImpl(SquadMemberRepository repository, SquadService squadService, PlayerService playerService) {
        super(repository);
        this.squadService = squadService;
        this.playerService = playerService;
    }

    @Override
    public long numberOfSquadMembersInSquadWithId(Long squadId) throws ResponseStatusException {
        if (!squadService.existsById(squadId)) throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Squad", squadId));
        return repository.countBySquadId(squadId);
    }

    @Override
    public long numberOfDeceasedSquadMembersInSquadWithId(Long squadId) throws ResponseStatusException {
        if (!squadService.existsById(squadId)) throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Squad", squadId));
        return repository.countBySquadIdAndPlayerIsHuman(squadId, false);
    }

    @Override
    public SquadMember getSquadMemberFromPlayerId(Long playerId) throws ResponseStatusException {
        if (!playerService.existsById(playerId)) throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Player", playerId));
        return repository.findByPlayerId(playerId).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Player with id " + playerId + " is not in a squad.");
        });
    }

    @Override
    public boolean inSquad(Long playerId) throws ResponseStatusException {
        if (!playerService.existsById(playerId)) throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Player", playerId));
        return repository.findByPlayerId(playerId).isPresent();
    }

    @Override
    public SquadMember create(SquadMember squadMember, Long gameId) throws ResponseStatusException {
        if (!squadMember.getSquad().getGame().getId().equals(gameId))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Squad not in current game.");
        if (inSquad(squadMember.getPlayer().getId()))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Already joined a squad.");
        if (!squadMember.getPlayer().getIsHuman())
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Zombies are not allowed to join squads.");
        return super.create(squadMember);
    }

    @Override
    public void deleteById(Long squadMemberId, Long squadId) throws ResponseStatusException {
        if (!this.findById(squadMemberId).getSquad().getId().equals(squadId))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Squad not in current game.");
        if (!this.findById(squadMemberId).getPlayer().getIsHuman())
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Zombies are not allowed to leave squads.");
        super.deleteById(squadMemberId);
    }
}
