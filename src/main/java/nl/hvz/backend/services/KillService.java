package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.Kill;
import nl.hvz.backend.models.entities.User;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public interface KillService extends CrudService<Kill,Long> {
    /**
     * Return all kills in a specific game.
     * @param gameId The id of the game.
     * @return All the kills in the given game, ordered by the time the kill took place.
     * @throws ResponseStatusException if the game does not exist.
     */
    List<Kill> findAll(Long gameId) throws ResponseStatusException;

    /**
     * Return a kill in a specific game.
     * @param killId The kill id.
     * @param gameId The game id.
     * @return The kill entity.
     * @throws ResponseStatusException If the game cannot be found in the game.
     */
    Kill findById(Long killId, Long gameId) throws ResponseStatusException;

    /**
     * @param killId The kill id of the kill to be deleted.
     * @param gameId The game id where the kill should be.
     * @throws ResponseStatusException If the kill is not in the game.
     */
    void deleteById(Long killId, Long gameId) throws ResponseStatusException;

    /**
     * Update a kill entity.
     * @param kill The updated kill entity
     * @param gameId The game id.
     * @param user The user that is going to update
     * @return The kill entity that will be persisted
     * @throws ResponseStatusException If the user is not allowed to modify the kill.
     */
    Kill update(Kill kill, Long gameId, User user) throws ResponseStatusException;
}
