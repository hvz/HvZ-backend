package nl.hvz.backend.services;

import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * General service interface that offers to perform some crud operations on a specific table.
 * @param <T> The entity type the table consists of.
 * @param <ID> The type of id the entity uses.
 */
public interface CrudService<T,ID> {
    String DOES_NOT_EXIST_ERROR_TEMPLATE = "%s with id %s does not exist.";

    /**
     * @return A list of all available entities.
     */
    List<T> findAll();

    /**
     * @param id The id of the entity to be found.
     * @return The entity with the requested id.
     * @throws ResponseStatusException If the entity cannot be found.
     */
    T findById(ID id) throws ResponseStatusException;

    /**
     * @param id The id of the entity to be found.
     * @return {@code true} if entity exists, {@code false} otherwise.
     */
    boolean existsById(ID id);

    /**
     * Updates <b>an existing entity</b>.
     * @param entity The updated entity.
     *               Note that this may differ from what will eventually be persisted.
     * @return The final entity that will be persisted.
     * @throws ResponseStatusException If the id of the entity that should be updated does not exist.
     */
    T update(T entity) throws ResponseStatusException;

    /**
     * Creates <b>a new entity</b>.
     * @param entity The entity to be added.
     *               Note that the this may differ from what will eventually be persisted.
     * @return The final entity that will be persisted.
     * @throws ResponseStatusException If an id is specified in the entity (even if it is a non-existing id).
     */
    T create(T entity) throws ResponseStatusException;

    /**
     * @param id The id of the entity to be deleted.
     */
    void deleteById(ID id);
}
