package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.SquadCheckin;
import nl.hvz.backend.models.entities.User;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public interface SquadCheckinService extends CrudService<SquadCheckin,Long> {

     /**
      * @param gameId The game id.
      * @param squadId The squad id.
      * @param user The current user doing the request.
      * @return A list of squad checkins in a game for a specific squad.
      * @throws ResponseStatusException If squad is not in the game.
      * @throws ResponseStatusException If the current user is not allowed to see the squad checkins.
      */
    List<SquadCheckin> findAll(Long gameId, Long squadId, User user) throws ResponseStatusException;

    /**
     * @param squadCheckin The squad checkin entity to be saved.
     * @param gameId The id of the same the checkin should be in.
     * @return The squad checkin entity after it has been persisted.
     * @throws ResponseStatusException If the squad of the checkin is not the given game.
     */
    SquadCheckin create(SquadCheckin squadCheckin, Long gameId) throws ResponseStatusException;
}
