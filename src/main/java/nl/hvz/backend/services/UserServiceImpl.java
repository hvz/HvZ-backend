package nl.hvz.backend.services;

import lombok.AllArgsConstructor;
import nl.hvz.backend.configurations.SecurityConfiguration;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.repositories.UserRepository;
import org.mapstruct.AfterMapping;
import org.mapstruct.MappingTarget;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Override
    public String getUsernameFromJwt(Jwt jwt) throws ResponseStatusException {
        Optional<String> username = Optional.ofNullable(jwt.getClaimAsString("nickname"));
        return username.orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No 'nickname' claim.");
        });
    }

    @Override
    public boolean getAdminRoleFromJwt(Jwt jwt) throws ResponseStatusException {
        Optional<List<String>> scope = Optional.ofNullable(jwt.getClaimAsStringList(SecurityConfiguration.TOKEN_NAMESPACE_ROLES));
        return scope.orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No appropriate 'roles' claim.");
        }).contains("ADMIN");
    }

    @Override
    public User persist(@MappingTarget User user) {
        return repository.save(user);
    }
}
