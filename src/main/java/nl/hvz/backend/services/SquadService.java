package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.Squad;
import nl.hvz.backend.models.entities.User;
import org.mapstruct.Named;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public interface SquadService extends CrudService<Squad,Long> {
    @Override
    @Named("findSquadById")
    Squad findById(Long id) throws ResponseStatusException;

    /**
     * Get details of a specific squad
     * @param squadId The squad id.
     * @param gameId The game id.
     * @param user The current user.
     * @return The squad entity.
     * @throws ResponseStatusException If the user is not allowed to view the squad entity.
     */
    Squad findById(Long squadId, Long gameId, User user) throws ResponseStatusException;

    /**
     * @param gameId The game id.
     * @param squadId The squad id.
     * @return {@code true} if the squad is in the game, {@code false} otherwise.
     * @throws ResponseStatusException If either game id or squad id does not exist.
     */
    boolean squadInGame(Long gameId, Long squadId) throws ResponseStatusException;

    /**
     * @param gameId The game id.
     * @return List of squads in game.
     * @throws ResponseStatusException If the game does not exist.
     */
    List<Squad> findAll (Long gameId) throws ResponseStatusException;

    /**
     * @param squad The squad entity to be persisted.
     * @param user The current user.
     * @return The squad entity that will be persisted.
     * @throws ResponseStatusException If the user is not allowed to add a new squad
     */
    Squad create(Squad squad, User user) throws ResponseStatusException;

    /**
     * Deletes the squad with the specified squad id.
     * @param squadId The squad id.
     * @param gameId The game where the squad should be in.
     * @throws ResponseStatusException If the squad is not in the game.
     */
    void deleteById(Long squadId, Long gameId) throws ResponseStatusException;
}
