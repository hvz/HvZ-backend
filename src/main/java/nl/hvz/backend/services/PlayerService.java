package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.User;
import org.mapstruct.Named;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public interface PlayerService extends CrudService<Player,Long> {
    @Override
    @Named("findPlayerById")
    Player findById(Long id) throws ResponseStatusException;

    /**
     * Get details of a player. This is only possible if you are the player
     * @param playerId The player id
     * @param gameId The id of the game the player should be in.
     * @param user The current user doing the request
     * @return The player entity
     * @throws ResponseStatusException If the current user is not allowed to see the details of the player.
     * @throws ResponseStatusException If the player is not in the game with the specified id.
     */
    Player findById(Long playerId, Long gameId, User user) throws ResponseStatusException;

    /**
     * Get details of a player. Assuming the user has admin rights.
     * @param playerId The player id
     * @param gameId The id of the game the player should be in.
     * @return The player entity
     * @throws ResponseStatusException If the player is not in the game with the specified id.
     */
    Player findById(Long playerId, Long gameId) throws ResponseStatusException;

    /**
     * @param gameId The game id.
     * @return The number of players in the game with the specified id.
     * @throws ResponseStatusException if the game cannot be found.
     */
    @Named("countPlayersByGameId")
    long numberOfPlayersInGameWithId(Long gameId) throws ResponseStatusException;

    /**
     * Generate a unique bite code for a new player participating in a specific game.
     * @param gameId The id of the game where the new player will be added.
     * @return The bite code for the new player
     * @throws ResponseStatusException If the game does not exist.
     */
    String generateUniqueBiteCode(Long gameId) throws ResponseStatusException;

    /**
     * Find the player in a game with a certain user id.
     * @param userId The user id.
     * @param gameId The game id.
     * @return The player found.
     * @throws ResponseStatusException if the player cannot be found.
     */
    Player findPlayerInGame(String userId, Long gameId) throws ResponseStatusException;

    /**
     * @param gameId The id of the game the kill takes place.
     * @param biteCode The bite code of the victim.
     * @return The victim player.
     * @throws ResponseStatusException If the victim cannot be determined.
     */
    Player getVictimByBiteCode(Long gameId, String biteCode) throws ResponseStatusException;

    /**
     * @param gameId The game id.
     * @return A list of players in a game.
     */
    List<Player> findAll(Long gameId);

    void deleteById(Long playerId, Long gameId);

    /**
     * @param player The player entity to be added.
     * @param gameId The game where the player should be added.
     * @param user The user that should be added.
     * @return The player entity that will eventually be persisted.
     * @throws ResponseStatusException If the player is already in the game.
     */
    Player create(Player player, Long gameId, User user) throws ResponseStatusException;
}
