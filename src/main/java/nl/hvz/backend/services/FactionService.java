package nl.hvz.backend.services;

import nl.hvz.backend.models.entities.FactionAssociatedEntity;
import nl.hvz.backend.models.entities.User;

import java.util.List;

/**
 * Service that involves logic with factions. Not associated with a particular repository.
 */
public interface FactionService<T extends FactionAssociatedEntity<?>> {
    /**
     * Filter a list by faction. When the user is admin, the list will not be filtered.
     * If the user is a player, missions not belonging to its faction will be filtered away.
     * @param input The input list to be filtered by faction.
     * @param gameId The game id.
     * @param user The user for which the list has to be filtered.
     * @return The filtered list.
     */
    List<T> filterByFaction(List<T> input, Long gameId, User user);
}
