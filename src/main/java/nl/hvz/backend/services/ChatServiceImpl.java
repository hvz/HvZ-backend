package nl.hvz.backend.services;

import nl.hvz.backend.models.Faction;
import nl.hvz.backend.models.entities.*;
import nl.hvz.backend.repositories.ChatRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class ChatServiceImpl extends CrudServiceImpl<ChatMessage, Long, ChatRepository> implements ChatService {
    private final GameService gameService;
    private final PlayerService playerService;
    private final FactionService<ChatMessage> factionService;
    private final SquadService squadService;
    private final SquadMemberService squadMemberService;

    public ChatServiceImpl(ChatRepository repository, GameService gameService, PlayerService playerService,
                           FactionService<ChatMessage> factionService, SquadService squadService,
                           SquadMemberService squadMemberService) {
        super(repository);
        this.gameService = gameService;
        this.playerService = playerService;
        this.factionService = factionService;
        this.squadService = squadService;
        this.squadMemberService = squadMemberService;
    }

    @Override
    public List<ChatMessage> findAll(Long gameId, Long squadId, User user) throws ResponseStatusException {
        if (!gameService.existsById(gameId)) throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Game", gameId));
        if (squadId != null) {
            if (!squadService.squadInGame(gameId, squadId))
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Squad not in current game.");
            if (!user.getIsAdmin()) {
                Player currentPlayer = playerService.findPlayerInGame(user.getId(), gameId);
                SquadMember squadMember = squadMemberService.getSquadMemberFromPlayerId(currentPlayer.getId());
                if (!squadId.equals(squadMember.getSquad().getId()))
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Current player not a member of the squad");
            }
        }
        List<ChatMessage> chatMessages = (squadId != null) ?
                repository.findByGameIdAndSquadId(gameId, squadId) :
                repository.findByGameId(gameId).stream().filter(m -> m.getSquad()==null).toList();
        return factionService.filterByFaction(chatMessages, gameId, user);
    }

    @Override
    public ChatMessage create(ChatMessage chatMessage, Long gameId, User user) throws ResponseStatusException {
        Squad squad = chatMessage.getSquad();
        if (squad != null && !squadService.squadInGame(gameId, chatMessage.getSquad().getId()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Squad is not in current game");
        if (!user.getIsAdmin()) {
            Player currentPlayer = playerService.findPlayerInGame(user.getId(), gameId);
            if (!chatMessage.getFaction().equals(Faction.GLOBAL)
                    && (chatMessage.getFaction().equals(Faction.HUMAN) != currentPlayer.getIsHuman()))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                        "Not allowed sent messages in human faction as zombie or vice versa.");
            if (squad != null) {
                SquadMember squadMember = squadMemberService.getSquadMemberFromPlayerId(currentPlayer.getId());
                if (!squad.getId().equals(squadMember.getSquad().getId()))
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Current player not a member of the squad");
            }
        }
        return super.create(chatMessage);
    }
}
