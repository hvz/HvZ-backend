package nl.hvz.backend.services;

import nl.hvz.backend.models.GameState;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.repositories.PlayerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

@Service
public class PlayerServiceImpl extends CrudServiceImpl<Player, Long, PlayerRepository> implements PlayerService {
    private final GameService gameService;
    public PlayerServiceImpl(PlayerRepository repository, GameService gameService) {
        super(repository);
        this.gameService = gameService;
    }

    @Override
    public Player getVictimByBiteCode(Long gameId, String biteCode) throws ResponseStatusException {
        return repository.findByBiteCodeAndGameId(biteCode, gameId).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Invalid bite code.");
        });
    }

    @Override
    public Player findById(Long playerId, Long gameId, User user) throws ResponseStatusException {
        if (!user.getIsAdmin()) {
            Long currentPlayerId = this.findPlayerInGame(user.getId(), gameId).getId();
            if (playerId.equals(0L))
                playerId = currentPlayerId;
            if (!currentPlayerId.equals(playerId)) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                        "Request for player details only allowed for admin and for the player themselves");
            }
        }
        Player player = super.findById(playerId);
        if (!player.getGame().getId().equals(gameId))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested player not in current game.");
        return player;
    }

    @Override
    public Player findById(Long playerId, Long gameId) throws ResponseStatusException {
        User user = new User();
        user.setIsAdmin(true);
        return this.findById(playerId, gameId, user);
    }

    @Override
    public long numberOfPlayersInGameWithId(Long gameId) throws ResponseStatusException {
        if (!gameService.existsById(gameId)) throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Game", gameId));
        return repository.countByGameId(gameId);
    }

    @Override
    public String generateUniqueBiteCode(Long gameId) throws ResponseStatusException {
        if (!gameService.existsById(gameId)) throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format(DOES_NOT_EXIST_ERROR_TEMPLATE, "Game", gameId));
        Set<String> biteCodes = repository.findBiteCodeByGameId(gameId);
        String result;
        do {
            result = new Random().ints(4,'a','a' + 35).mapToObj(i -> {
                if (i >= 'o') ++i; // The letter 'o' will not be used, because it resembles the 0 too much.
                if (i > 'z') i += '0' - 'z' - 1;
                return (char) i;
            }).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
        } while (biteCodes.contains(result));
        return result;
    }

    @Override
    public Player findPlayerInGame(String userId, Long gameId) throws ResponseStatusException {
        return repository.findByUserIdAndGameId(userId, gameId).orElseThrow(() -> {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                    "The current user does not participate in the current game.");
        });
    }

    @Override
    public List<Player> findAll(Long gameId) {
        return repository.findByGameId(gameId);
    }

    @Override
    public void deleteById(Long playerId, Long gameId) {
        this.findById(playerId, gameId);
        super.deleteById(playerId);
    }

    @Override
    public Player create(Player player, Long gameId, User user) throws ResponseStatusException {
        if (user.getIsAdmin())
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No cheaters with admin rights allowed.");
        if (!gameService.findById(gameId).getGameState().equals(GameState.REGISTRATION))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Registration new players closed.");
        Optional<Player> alreadyExistingPlayer = repository.findByUserIdAndGameId(user.getId(), gameId);
        return alreadyExistingPlayer.orElseGet(() -> super.create(player));
    }
}
