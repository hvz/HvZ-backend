package nl.hvz.backend.mappers;

import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.UserService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.security.oauth2.jwt.Jwt;

@Mapper(componentModel = "spring", uses = UserService.class,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface UserMapper {
    @Mapping(target = "id", source = "jwt.subject")
    @Mapping(target = "username", source = "jwt", qualifiedByName = "toUsernameFromJwt")
    @Mapping(target = "isAdmin", source = "jwt", qualifiedByName = "toIsAdminFromJwt")
    @Mapping(target = "players", ignore = true)
    @Mapping(target = "chatMessages", ignore = true)
    User getUserByJwt(Jwt jwt);
}
