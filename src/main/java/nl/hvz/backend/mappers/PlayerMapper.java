package nl.hvz.backend.mappers;

import nl.hvz.backend.models.dtos.playerdtos.ModifyPlayerDto;
import nl.hvz.backend.models.dtos.playerdtos.PlayerDetailsDto;
import nl.hvz.backend.models.dtos.playerdtos.PlayerListItemDto;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.GameService;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {GameService.class},
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PlayerMapper {
    @IterableMapping(qualifiedByName = "PlayerListItemDtoMapping")
    List<PlayerListItemDto> toPlayerListItemCollectionDto(List<Player> players);

    @Named("PlayerListItemDtoMapping")
    @Mapping(target = "username", source = "user.username")
    PlayerListItemDto toPlayerListItemDto(Player player);

    @Mapping(target = "username", source = "user.username")
    PlayerDetailsDto toPlayerDetailsDto(Player player);

    @Mapping(target = "squadMember", ignore = true)
    @Mapping(target = "kills", ignore = true)
    @Mapping(target = "isPatientZero", constant = "false")
    @Mapping(target = "isHuman", constant = "true")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "game", source = "gameId", qualifiedByName = "findGameById")
    @Mapping(target = "deathCause", ignore = true)
    Player toNewPlayerEntity(Long gameId, User user, String biteCode);

    @Mapping(target = "user", ignore = true)
    @Mapping(target = "squadMember", ignore = true)
    @Mapping(target = "kills", ignore = true)
    @Mapping(target = "game", ignore = true)
    @Mapping(target = "deathCause", ignore = true)
    @Mapping(target = "biteCode", ignore = true)
    Player toUpdatePlayerEntity(ModifyPlayerDto modifyPlayerDto, Long id, @MappingTarget Player player);
}
