package nl.hvz.backend.mappers;

import nl.hvz.backend.models.GameState;
import nl.hvz.backend.models.dtos.gamedtos.GameDetailsDto;
import nl.hvz.backend.models.dtos.gamedtos.GameListItemDto;
import nl.hvz.backend.models.dtos.gamedtos.ModifyGameDto;
import nl.hvz.backend.models.dtos.gamedtos.NewGameDto;
import nl.hvz.backend.models.entities.Game;
import nl.hvz.backend.services.PlayerService;
import org.mapstruct.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Mapper(componentModel = "spring", uses = PlayerService.class,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface GameMapper {
    @IterableMapping(qualifiedByName = "GameListItemDtoMapping")
    List<GameListItemDto> toGameListItemCollectionDto(List<Game> games);

    @Named("GameListItemDtoMapping")
    @Mapping(target = "numberOfPlayers", source = "id", qualifiedByName = "countPlayersByGameId")
    GameListItemDto toGameListItemDto(Game game);

    @Mapping(target = "numberOfPlayers", source = "id", qualifiedByName = "countPlayersByGameId")
    GameDetailsDto toGameDetailsDto(Game game);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "squads", ignore = true)
    @Mapping(target = "published", ignore = true)
    @Mapping(target = "players", ignore = true)
    @Mapping(target = "missions", ignore = true)
    @Mapping(target = "gameState", constant = "REGISTRATION")
    @Mapping(target = "chatMessages", ignore = true)
    @Mapping(target = "center.id", ignore = true)
    Game toNewGameEntity(NewGameDto newGameDto);

    @Mapping(target = "squads", ignore = true)
    @Mapping(target = "published", ignore = true)
    @Mapping(target = "players", ignore = true)
    @Mapping(target = "missions", ignore = true)
    @Mapping(target = "chatMessages", ignore = true)
    @Mapping(target = "gameState", source = "modifyGameDto.gameState", qualifiedByName = "toGameStateEnum")
    @Mapping(target = "center.id", ignore = true)
    Game toUpdateGameEntity(ModifyGameDto modifyGameDto, Long id, @MappingTarget Game game);
    
    @Named("toGameStateEnum")
    default GameState toGameStateEnum(String gameState) {
        try {
            return GameState.valueOf(gameState);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "gameState must be 'REGISTRATION', 'IN_PROGRESS', or 'COMPLETED'.", e);
        }
    }
}
