package nl.hvz.backend.mappers;

import nl.hvz.backend.models.dtos.squadcheckindtos.NewSquadCheckinDto;
import nl.hvz.backend.models.dtos.squadcheckindtos.SquadCheckinInfoDto;
import nl.hvz.backend.models.entities.SquadCheckin;
import nl.hvz.backend.models.entities.SquadMember;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SquadCheckinMapper {
    @IterableMapping(qualifiedByName = "SquadCheckinInfoDtoMapping")
    List<SquadCheckinInfoDto> toSquadCheckinInfoCollectionDto(List<SquadCheckin> squadCheckins);

    @Named("SquadCheckinInfoDtoMapping")
    @Mapping(target = "username", source = "squadMember.player.user.username")
    @Mapping(target = "squadMemberId", source = "squadMember.id")
    @Mapping(target = "playerId", source = "squadMember.player.id")
    SquadCheckinInfoDto toSquadCheckinInfoDto(SquadCheckin squadCheckin);

    @Mapping(target = "timestamp", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "location.id", ignore = true)
    SquadCheckin toNewSquadCheckinEntity(NewSquadCheckinDto newSquadCheckinDto, SquadMember squadMember);
}
