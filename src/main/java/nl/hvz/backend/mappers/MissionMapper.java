package nl.hvz.backend.mappers;

import nl.hvz.backend.models.dtos.missiondtos.MissionDetailsDto;
import nl.hvz.backend.models.dtos.missiondtos.MissionListItemDto;
import nl.hvz.backend.models.dtos.missiondtos.ModifyMissionDto;
import nl.hvz.backend.models.dtos.missiondtos.NewMissionDto;
import nl.hvz.backend.models.entities.Mission;
import nl.hvz.backend.services.GameService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring", uses = GameService.class,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface MissionMapper {
    List<MissionListItemDto> toMissionListItemCollectionDto(List<Mission> missions);

    MissionDetailsDto toMissionDetailsDto(Mission mission);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "game", source = "gameId", qualifiedByName = "findGameById")
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "marker.id", ignore = true)
    Mission toNewMissionEntity(NewMissionDto newMissionDto, Long gameId);

    @Mapping(target = "id", source = "missionId")
    @Mapping(target = "game", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "marker.id", ignore = true)
    Mission toUpdateMissionEntity(ModifyMissionDto modifyMissionDto, Long missionId, @MappingTarget Mission mission);
}
