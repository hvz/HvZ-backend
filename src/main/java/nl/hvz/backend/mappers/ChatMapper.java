package nl.hvz.backend.mappers;

import nl.hvz.backend.models.dtos.chatmessagedto.ChatMessageInfoDto;
import nl.hvz.backend.models.dtos.chatmessagedto.NewChatMessageDto;
import nl.hvz.backend.models.entities.ChatMessage;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.GameService;
import nl.hvz.backend.services.SquadService;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {SquadService.class, GameService.class},
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ChatMapper {
    @IterableMapping(qualifiedByName = "ChatMessageInfoDtoMapping")
    List<ChatMessageInfoDto> toChatMessageInfoCollectionDto(List<ChatMessage> chatMessages);

    @Named("ChatMessageInfoDtoMapping")
    @Mapping(target = "username", source = "messageOwner.username")
    @Mapping(target = "isAdmin", source = "messageOwner.isAdmin")
    ChatMessageInfoDto toChatMessageInfoDto(ChatMessage chatMessages);

    @Mapping(target = "timestamp", ignore = true)
    @Mapping(target = "squad", source = "squadId", qualifiedByName = "findSquadById")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "game", source = "gameId", qualifiedByName = "findGameById")
    ChatMessage toNewChatMessageEntity(NewChatMessageDto newChatMessageDto, Long gameId, Long squadId,
                                       User messageOwner);
}
