package nl.hvz.backend.mappers;

import nl.hvz.backend.models.dtos.killdtos.KillDetailsDto;
import nl.hvz.backend.models.dtos.killdtos.KillListItemDto;
import nl.hvz.backend.models.dtos.killdtos.ModifyKillDto;
import nl.hvz.backend.models.dtos.killdtos.NewKillDto;
import nl.hvz.backend.models.entities.Kill;
import nl.hvz.backend.services.PlayerService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

@Mapper(componentModel = "spring", uses = {PlayerService.class},
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface KillMapper {
    List<KillListItemDto> toKillListItemCollectionDto(List<Kill> kills);

    @Mapping(target = "victimUsername", source = "victim.user.username")
    @Mapping(target = "victimId", source = "victim.id")
    @Mapping(target = "killerUsername", source = "killer.user.username")
    @Mapping(target = "killerId", source = "killer.id")
    KillDetailsDto toKillDetailsDto(Kill kill);

    // IMPORTANT: the victim and killer will be used to map (not the bite code)
    @Mapping(target = "victim", source = "newKillDto.victimId", qualifiedByName = "findPlayerById")
    @Mapping(target = "timeOfDeath", ignore = true)
    @Mapping(target = "killer", source = "newKillDto.killerId", qualifiedByName = "findPlayerById")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "location.id", ignore = true)
    Kill toNewKillEntity(NewKillDto newKillDto);

    @Mapping(target = "victim", source = "modifyKillDto.victimId", qualifiedByName = "findPlayerById")
    @Mapping(target = "timeOfDeath", ignore = true)
    @Mapping(target = "killer", source = "modifyKillDto.killerId", qualifiedByName = "findPlayerById")
    @Mapping(target = "location.id", ignore = true)
    Kill toUpdateKillEntity(ModifyKillDto modifyKillDto, Long id, @MappingTarget Kill kill);
}
