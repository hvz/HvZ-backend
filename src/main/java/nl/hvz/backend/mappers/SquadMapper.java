package nl.hvz.backend.mappers;

import nl.hvz.backend.models.dtos.squaddtos.*;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.Squad;
import nl.hvz.backend.models.entities.SquadMember;
import nl.hvz.backend.services.*;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = {GameService.class, SquadService.class, SquadMemberService.class},
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SquadMapper {
    @IterableMapping(qualifiedByName = "SquadListItemDtoMapping")
    List<SquadListItemDto> toSquadListItemCollectionDto(List<Squad> squads);

    @Named("SquadListItemDtoMapping")
    @Mapping(target = "numberOfMembers", source = "squad.id", qualifiedByName = "numberOfSquadMembers")
    @Mapping(target = "numberOfDeceased", source = "squad.id", qualifiedByName = "numberOfDeceasedSquadMembers")
    SquadListItemDto toSquadListItemDto(Squad squad);

    @Mapping(target = "squadMembers", source = "squadMembers", qualifiedByName = "SquadMemberCollectionDtoMapping")
    SquadDetailsDto toSquadDetailsDto(Squad squad);

    @Named("SquadMemberCollectionDtoMapping")
    @IterableMapping(qualifiedByName = "SquadMemberDtoMapping")
    List<SquadMemberDto> toSquadMemberCollectionDto(List<SquadMember> squadMembers);

    @Named("SquadMemberDtoMapping")
    @Mapping(target = "username", source = "player.user.username")
    @Mapping(target = "playerId", source = "player.id")
    @Mapping(target = "isHuman", source = "player.isHuman")
    SquadMemberDto toSquadMemberDto(SquadMember squadMember);

    @Mapping(target = "squadMembers", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "game", source = "gameId", qualifiedByName = "findGameById")
    @Mapping(target = "chatMessages", ignore = true)
    Squad toNewSquadEntity(NewSquadDto newSquadDto, Long gameId);

    @Mapping(target = "squadCheckins", ignore = true)
    @Mapping(target = "squad", source = "squadId", qualifiedByName = "findSquadById")
    @Mapping(target = "joinedAt", ignore = true)
    @Mapping(target = "id", ignore = true)
    SquadMember toNewSquadMemberEntity(Long squadId, Player player);

    @Mapping(target = "squadMembers", ignore = true)
    @Mapping(target = "game", ignore = true)
    @Mapping(target = "chatMessages", ignore = true)
    Squad toUpdateSquadEntity(ModifySquadDto modifySquadDto, Long id, @MappingTarget Squad squad);
}
