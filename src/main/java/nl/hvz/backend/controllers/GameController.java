package nl.hvz.backend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import nl.hvz.backend.mappers.GameMapper;
import nl.hvz.backend.models.dtos.gamedtos.GameDetailsDto;
import nl.hvz.backend.models.dtos.gamedtos.GameListItemDto;
import nl.hvz.backend.models.dtos.gamedtos.ModifyGameDto;
import nl.hvz.backend.models.dtos.gamedtos.NewGameDto;
import nl.hvz.backend.models.entities.Game;
import nl.hvz.backend.services.GameService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/game")
@CrossOrigin(origins = {
        "http://localhost:4200",
        "https://ng-hvz.herokuapp.com"
})
@AllArgsConstructor
public class GameController {
    private final GameService gameService;
    private final GameMapper gameMapper;

    @Operation(summary = "Add new game")
    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<GameDetailsDto> create(@RequestBody NewGameDto newGameDto) {
        Game newGame = gameService.create(gameMapper.toNewGameEntity(newGameDto));
        URI uri = URI.create("api/v1/game/" + newGame.getId());
        return ResponseEntity.created(uri).body(
                gameMapper.toGameDetailsDto(newGame));
    }

    @Operation(summary= "Get all games")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Games not found",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<Collection<GameListItemDto>> findAll() {
        Collection<GameListItemDto> gameListItemDto = gameMapper.toGameListItemCollectionDto(
                gameService.findAll()
        );
        return ResponseEntity.ok(gameListItemDto);
    }

    @Operation(summary= "Get game by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Game not found with supplied ID",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<GameDetailsDto> findById(@PathVariable Long id) {
        GameDetailsDto gameDetailsDto = gameMapper.toGameDetailsDto(
                gameService.findById(id)
        );
        return ResponseEntity.ok(gameDetailsDto);
    }

    @Operation(summary = "Updates a game")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Game successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Game not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<GameDetailsDto> updateById(@RequestBody ModifyGameDto modifyGameDto, @PathVariable Long id) {
        Game game = gameService.findById(id);
        Game updatedGame = gameService.update(
                gameMapper.toUpdateGameEntity(modifyGameDto, id, game)
        );
        return ResponseEntity.ok(gameMapper.toGameDetailsDto(updatedGame));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        gameService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
