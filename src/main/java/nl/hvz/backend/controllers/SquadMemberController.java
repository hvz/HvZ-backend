package nl.hvz.backend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import nl.hvz.backend.mappers.SquadMapper;
import nl.hvz.backend.mappers.UserMapper;
import nl.hvz.backend.models.dtos.squaddtos.SquadMemberDto;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.SquadMember;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.PlayerService;
import nl.hvz.backend.services.SquadMemberService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = {
        "http://localhost:4200",
        "https://ng-hvz.herokuapp.com"
})
@RequestMapping(path = "api/v1/game/{gameId}/squad/{squadId}")
public class SquadMemberController {

    private final SquadMemberService squadMemberService;
    private final SquadMapper squadMapper;
    private final UserMapper userMapper;
    private final PlayerService playerService;

    @Operation(summary = "Add new squad member")
    @PostMapping("join")
    public ResponseEntity<SquadMemberDto> create(@PathVariable Long gameId,
                                                 @PathVariable Long squadId,
                                                 @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Player player = playerService.findPlayerInGame(user.getId(), gameId);
        SquadMember newSquadMember = squadMemberService.create(
                squadMapper.toNewSquadMemberEntity(squadId, player),
                gameId
        );
        URI uri = URI.create("api/v1/game/" + gameId + "/squad/" + squadId);
        return ResponseEntity.created(uri).body(
                squadMapper.toSquadMemberDto(newSquadMember)
        );
    }

    @DeleteMapping("leave")
    public ResponseEntity<?> delete(@PathVariable Long gameId,
                                 @PathVariable Long squadId,
                                 @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Long playerId = playerService.findPlayerInGame(user.getId(), gameId).getId();
        Long squadMemberId = squadMemberService.getSquadMemberFromPlayerId(playerId).getId();
        squadMemberService.deleteById(squadMemberId, squadId);
        return ResponseEntity.noContent().build();
    }
}
