package nl.hvz.backend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import nl.hvz.backend.mappers.SquadMapper;
import nl.hvz.backend.mappers.UserMapper;
import nl.hvz.backend.models.dtos.squaddtos.*;
import nl.hvz.backend.models.entities.Squad;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.PlayerService;
import nl.hvz.backend.services.SquadMemberService;
import nl.hvz.backend.services.SquadService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = {
        "http://localhost:4200",
        "https://ng-hvz.herokuapp.com"
})
@RequestMapping(path = "api/v1/game/{gameId}/squad")
public class SquadController {

    private final SquadService squadService;
    private final PlayerService playerService;
    private final SquadMemberService squadMemberService;
    private final SquadMapper squadMapper;
    private final UserMapper userMapper;
    private final SquadMemberController squadMemberController;

    @Operation(summary = "Add new squad")
    @PostMapping
    public ResponseEntity<SquadDetailsDto> create(@RequestBody NewSquadDto newSquadDto,
                                 @PathVariable Long gameId,
                                 @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Squad newSquad = squadService.create(
                squadMapper.toNewSquadEntity(newSquadDto, gameId),
                user
        );
        SquadDetailsDto squadDetailsDto = squadMapper.toSquadDetailsDto(newSquad);
        if (!user.getIsAdmin()) {
            SquadMemberDto squadMemberDto = squadMemberController.create(gameId, newSquad.getId(), jwt).getBody();
            squadDetailsDto.getSquadMembers().add(squadMemberDto);
        }
        URI uri = URI.create("api/v1/game/" + gameId + "/squad/" + newSquad.getId());
        return ResponseEntity.created(uri).body(squadDetailsDto);
    }

    @Operation(summary= "Get all squads")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Squads not found",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<Collection<SquadListItemDto>> findAll(@PathVariable Long gameId) {
        Collection<SquadListItemDto> squadListItemDtos = squadMapper.toSquadListItemCollectionDto(
                squadService.findAll(gameId)
        );
        return ResponseEntity.ok(squadListItemDtos);
    }

    @Operation(summary= "Get squad by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Squad not found with supplied ID",
                    content = @Content)
    })
    @GetMapping("{squadId}")
    public ResponseEntity<SquadDetailsDto> findById(@PathVariable Long gameId, @PathVariable Long squadId,
                                                    @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        if (squadId.equals(0L)) {
            Long currentPlayerId = playerService.findPlayerInGame(user.getId(), gameId).getId();
            squadId = squadMemberService.getSquadMemberFromPlayerId(currentPlayerId).getSquad().getId();
        }
        SquadDetailsDto squadDetailsDto = squadMapper.toSquadDetailsDto(
                squadService.findById(squadId, gameId, user)
        );
        return ResponseEntity.ok(squadDetailsDto);
    }

    @Operation(summary = "Updates a squad")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Squad successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Squad not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{squadId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<SquadDetailsDto> updateById(@RequestBody ModifySquadDto modifySquadDto,
                                     @PathVariable Long gameId,
                                     @PathVariable Long squadId,
                                     @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Squad squad = squadService.findById(squadId, gameId, user);
        Squad updateSquad = squadService.update(
                squadMapper.toUpdateSquadEntity(modifySquadDto, squadId, squad)
        );
        return ResponseEntity.ok(squadMapper.toSquadDetailsDto(updateSquad));
    }

    @DeleteMapping("{squadId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> delete (@PathVariable Long gameId,
                                     @PathVariable Long squadId) {
        squadService.deleteById(squadId, gameId);
        return ResponseEntity.noContent().build();
    }
}
