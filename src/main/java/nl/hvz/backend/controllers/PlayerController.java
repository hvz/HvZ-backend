package nl.hvz.backend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import nl.hvz.backend.mappers.PlayerMapper;
import nl.hvz.backend.mappers.UserMapper;
import nl.hvz.backend.models.dtos.playerdtos.ModifyPlayerDto;
import nl.hvz.backend.models.dtos.playerdtos.PlayerDetailsDto;
import nl.hvz.backend.models.dtos.playerdtos.PlayerListItemDto;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.PlayerService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = {
        "http://localhost:4200",
        "https://ng-hvz.herokuapp.com"
})
@RequestMapping(path = "/api/v1/game/{gameId}/player")
public class PlayerController {
    private final PlayerService playerService;
    private final PlayerMapper playerMapper;
    private final UserMapper userMapper;


    @Operation(summary = "Add new player")
    @PostMapping
    public ResponseEntity<PlayerDetailsDto> create(@PathVariable Long gameId,
                                                   @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        String biteCode = playerService.generateUniqueBiteCode(gameId);
        Player newPlayer = playerService.create(playerMapper.toNewPlayerEntity(gameId, user, biteCode), gameId, user);
        URI uri = URI.create("api/v1/game/" + gameId + "/player/" + newPlayer.getId());
        return ResponseEntity.created( uri ).body(playerMapper.toPlayerDetailsDto(newPlayer));
    }

    @Operation(summary= "Get all players")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Players not found",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<Collection<PlayerListItemDto>> findAll(@PathVariable Long gameId) {
        Collection<PlayerListItemDto> playerListItemDto = playerMapper.toPlayerListItemCollectionDto(
                playerService.findAll(gameId)
        );
        return ResponseEntity.ok(playerListItemDto);
    }

    @Operation(summary= "Get player by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Player not found with supplied ID",
                    content = @Content)
    })
    @GetMapping("{playerId}")
    public ResponseEntity<PlayerDetailsDto> findById(@PathVariable Long playerId,
                                                     @PathVariable Long gameId,
                                                     @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Player player = playerService.findById(playerId, gameId, user);
        PlayerDetailsDto playerDetailsDto = playerMapper.toPlayerDetailsDto(player);
        return ResponseEntity.ok(playerDetailsDto);
    }

    @Operation(summary = "Updates a player")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Player successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Player not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{playerId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<PlayerDetailsDto> updateById(@RequestBody ModifyPlayerDto modifyPlayerDto,
                                                       @PathVariable Long gameId,
                                                       @PathVariable Long playerId) {
        Player player = playerService.findById(playerId, gameId);
        Player updatePlayer = playerService.update(
                playerMapper.toUpdatePlayerEntity(modifyPlayerDto, playerId, player)
        );
        return ResponseEntity.ok(playerMapper.toPlayerDetailsDto(updatePlayer));
    }

    @DeleteMapping("{playerId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable Long playerId, @PathVariable Long gameId) {
        playerService.deleteById(playerId, gameId);
        return ResponseEntity.noContent().build();
    }
}
