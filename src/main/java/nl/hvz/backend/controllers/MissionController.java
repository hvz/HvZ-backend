package nl.hvz.backend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import nl.hvz.backend.mappers.MissionMapper;
import nl.hvz.backend.mappers.UserMapper;
import nl.hvz.backend.models.dtos.missiondtos.MissionDetailsDto;
import nl.hvz.backend.models.dtos.missiondtos.MissionListItemDto;
import nl.hvz.backend.models.dtos.missiondtos.ModifyMissionDto;
import nl.hvz.backend.models.dtos.missiondtos.NewMissionDto;
import nl.hvz.backend.models.entities.Mission;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.MissionService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = {
        "http://localhost:4200",
        "https://ng-hvz.herokuapp.com"
})
@RequestMapping(path = "api/v1/game/{gameId}/mission")
public class MissionController {

    private final MissionService missionService;
    private final MissionMapper missionMapper;
    private final UserMapper userMapper;

    @Operation(summary = "Add new mission")
    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<MissionDetailsDto> create(@RequestBody NewMissionDto newMissionDto, @PathVariable Long gameId) {
        Mission newMission = missionService.create(
                missionMapper.toNewMissionEntity(newMissionDto, gameId));
        URI uri = URI.create("api/v1/game/" + gameId + "/mission/" + newMission.getId());
        return ResponseEntity.created(uri).body(
                missionMapper.toMissionDetailsDto(newMission));
    }

    @Operation(summary= "Get all missions")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Missions not found",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity <Collection<MissionListItemDto>> findAll(@PathVariable Long gameId,
                                                                   @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Collection<MissionListItemDto> missionListItemDtos = missionMapper.toMissionListItemCollectionDto(
                missionService.findAll(gameId, user)
        );
        return ResponseEntity.ok(missionListItemDtos);
    }

    @Operation(summary= "Get mission by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Mission not found with supplied ID",
                    content = @Content)
    })
    @GetMapping("{missionId}")
    public ResponseEntity<MissionDetailsDto> findById(@PathVariable Long missionId,
                                                      @PathVariable Long gameId,
                                                      @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Mission mission = missionService.findById(missionId, gameId, user);
        MissionDetailsDto missionDetailsDto = missionMapper.toMissionDetailsDto(mission);
        return ResponseEntity.ok(missionDetailsDto);
    }

    @Operation(summary = "Updates a mission")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Mission successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Mission not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{missionId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<MissionDetailsDto> updateById(@RequestBody ModifyMissionDto modifyMissionDto,
                                                        @PathVariable Long gameId,
                                                        @PathVariable Long missionId,
                                                        @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Mission mission = missionService.findById(missionId, gameId, user);
        Mission updateMission = missionService.update(
                missionMapper.toUpdateMissionEntity(modifyMissionDto, missionId, mission)
        );
        return ResponseEntity.ok(missionMapper.toMissionDetailsDto(updateMission));
    }

    @DeleteMapping("{missionId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable Long gameId,
                                    @PathVariable Long missionId,
                                    @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        missionService.deleteById(missionId, gameId, user);
        return ResponseEntity.noContent().build();
    }
}
