package nl.hvz.backend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import nl.hvz.backend.mappers.ChatMapper;
import nl.hvz.backend.mappers.UserMapper;
import nl.hvz.backend.models.dtos.chatmessagedto.ChatMessageInfoDto;
import nl.hvz.backend.models.dtos.chatmessagedto.NewChatMessageDto;
import nl.hvz.backend.models.entities.ChatMessage;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.ChatService;
import nl.hvz.backend.services.SquadService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = {
        "http://localhost:4200",
        "https://ng-hvz.herokuapp.com"
})
@RequestMapping(path = "api/v1/game/{gameId}/squad/{squadId}/chat")
public class SquadChatMessageController {

    private final ChatService chatMessageService;
    private final ChatMapper chatMapper;
    private final UserMapper userMapper;

    @Operation(summary = "Add new chat message")
    @PostMapping
    public ResponseEntity<ChatMessageInfoDto> create(@RequestBody NewChatMessageDto newChatMessageDto,
                                                     @PathVariable Long gameId,
                                                     @PathVariable Long squadId,
                                                     @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        ChatMessage newChatMessage = chatMessageService.create(
                chatMapper.toNewChatMessageEntity(newChatMessageDto, gameId, squadId, user), gameId, user);
        URI uri = URI.create("api/v1/game/" + gameId + "/squad/" + squadId + "/chat");
        return ResponseEntity.created(uri).body(chatMapper.toChatMessageInfoDto(newChatMessage));
    }

    @Operation(summary= "Get all chat messages")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Chat messages not found",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<Collection<ChatMessageInfoDto>> findAll(@PathVariable Long gameId,
                                                                  @PathVariable Long squadId,
                                                                  @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Collection<ChatMessageInfoDto> chatMessageInfoDto = chatMapper.toChatMessageInfoCollectionDto(
                chatMessageService.findAll(gameId, squadId, user)
        );
        return ResponseEntity.ok(chatMessageInfoDto);
    }
}
