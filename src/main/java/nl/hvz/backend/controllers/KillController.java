package nl.hvz.backend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import nl.hvz.backend.mappers.KillMapper;
import nl.hvz.backend.mappers.UserMapper;
import nl.hvz.backend.models.dtos.killdtos.KillDetailsDto;
import nl.hvz.backend.models.dtos.killdtos.KillListItemDto;
import nl.hvz.backend.models.dtos.killdtos.ModifyKillDto;
import nl.hvz.backend.models.dtos.killdtos.NewKillDto;
import nl.hvz.backend.models.entities.Kill;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.KillService;
import nl.hvz.backend.services.PlayerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/game/{gameId}/kill")
@CrossOrigin(origins = {
        "http://localhost:4200",
        "https://ng-hvz.herokuapp.com"
})
@AllArgsConstructor
public class KillController {
    private final KillService killService;
    private final KillMapper killMapper;
    private final UserMapper userMapper;
    private final PlayerService playerService;

    @Operation(summary = "Add new kill")
    @PostMapping
    public ResponseEntity<KillDetailsDto> create(@RequestBody NewKillDto newKillDto,
                                                 @PathVariable Long gameId,
                                                 @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        if (!user.getIsAdmin()) {
            Player killer = playerService.findPlayerInGame(user.getId(), gameId);
            Player victim = playerService.getVictimByBiteCode(gameId, newKillDto.getBiteCode());
            newKillDto.setKillerId(killer.getId());
            newKillDto.setVictimId(victim.getId());
        }
        Kill newKill = killService.create(
                killMapper.toNewKillEntity(newKillDto));
        URI uri = URI.create("api/v1/game/" + gameId + "/kill/" + newKill.getId());
        return ResponseEntity.created(uri).body(killMapper.toKillDetailsDto(newKill));
    }

    @Operation(summary= "Get all kills")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content ),
            @ApiResponse(responseCode = "404",
                    description = "Kills not found",
                    content = @Content )
    })
    @GetMapping
    public ResponseEntity<Collection<KillListItemDto>> findAll(@PathVariable Long gameId) {
        Collection<KillListItemDto> killListItemDto = killMapper.toKillListItemCollectionDto(
                killService.findAll(gameId)
        );
        return ResponseEntity.ok(killListItemDto);
    }

    @Operation(summary= "Get kill by Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Kill not found with supplied ID",
                    content = @Content)
    })
    @GetMapping("{killId}")
    public ResponseEntity<KillDetailsDto> findById(@PathVariable Long gameId,
                                                   @PathVariable Long killId) {
        Kill kill = killService.findById(killId, gameId);
        KillDetailsDto killDetailsDto = killMapper.toKillDetailsDto(kill);
        return ResponseEntity.ok(killDetailsDto);
    }

    @Operation(summary = "Updates a kill")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Kill successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404",
                    description = "Kill not found with supplied ID",
                    content = {@Content(mediaType = "application/json")})
    })

    @PutMapping("{killId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<KillDetailsDto> updateById(@RequestBody ModifyKillDto modifyKillDto,
                                                     @PathVariable Long gameId,
                                                     @PathVariable Long killId,
                                                     @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        if (!user.getIsAdmin() && (modifyKillDto.getKillerId() != null || modifyKillDto.getVictimId() != null))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                    "Not allowed as non-admin to change killer or victim of kill");
        Kill kill = killService.findById(killId, gameId);
        Kill updateKill = killService.update(
                killMapper.toUpdateKillEntity(modifyKillDto, killId, kill), gameId, user
        );
        return ResponseEntity.ok(killMapper.toKillDetailsDto(updateKill));
    }

    @DeleteMapping("{killId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable Long killId, @PathVariable Long gameId) {
        killService.deleteById(killId, gameId);
        return ResponseEntity.noContent().build();
    }
}
