package nl.hvz.backend.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import nl.hvz.backend.mappers.SquadCheckinMapper;
import nl.hvz.backend.mappers.UserMapper;
import nl.hvz.backend.models.dtos.squadcheckindtos.NewSquadCheckinDto;
import nl.hvz.backend.models.dtos.squadcheckindtos.SquadCheckinInfoDto;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.SquadCheckin;
import nl.hvz.backend.models.entities.SquadMember;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.PlayerService;
import nl.hvz.backend.services.SquadCheckinService;
import nl.hvz.backend.services.SquadMemberService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/game/{gameId}/squad/{squadId}/checkin")
@CrossOrigin(origins = {
        "http://localhost:4200",
        "https://ng-hvz.herokuapp.com"
})
@AllArgsConstructor
public class SquadCheckinController {

    private final SquadCheckinService squadCheckinService;
    private final SquadCheckinMapper squadCheckinMapper;
    private final UserMapper userMapper;
    private final PlayerService playerService;
    private final SquadMemberService squadMemberService;

    @Operation(summary = "Add new squad checkin")
    @PostMapping
    public ResponseEntity<SquadCheckinInfoDto> create(@RequestBody NewSquadCheckinDto newSquadCheckinDto,
                                 @PathVariable Long squadId,
                                 @PathVariable Long gameId,
                                 @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        if (user.getIsAdmin())
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Admin cannot add new squad checkin.");
        Player currentPlayer = playerService.findPlayerInGame(user.getId(), gameId);
        SquadMember squadMember = squadMemberService.getSquadMemberFromPlayerId(currentPlayer.getId());
        SquadCheckin newSquadCheckin = squadCheckinService.create(
                squadCheckinMapper.toNewSquadCheckinEntity(newSquadCheckinDto, squadMember),
                gameId
        );
        URI uri = URI.create("api/v1/game/" + gameId + "/squad/" + squadId + "/checkin");
        return ResponseEntity.created(uri).body(squadCheckinMapper.toSquadCheckinInfoDto(newSquadCheckin));
    }

    @Operation(summary= "Get all squad checkins")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Squad checkins not found",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<Collection<SquadCheckinInfoDto>> findAll(@PathVariable Long gameId,
                                                                   @PathVariable Long squadId,
                                                                   @AuthenticationPrincipal Jwt jwt) {
        User user = userMapper.getUserByJwt(jwt);
        Collection<SquadCheckinInfoDto> squadCheckinListItemDto = squadCheckinMapper.toSquadCheckinInfoCollectionDto(
                squadCheckinService.findAll(gameId, squadId, user)
        );
        return ResponseEntity.ok(squadCheckinListItemDto);
    }
}
