package nl.hvz.backend.models.entities;

import nl.hvz.backend.models.GameState;
import nl.hvz.backend.repositories.GameRepository;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZonedDateTime;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class JpaEntityTests {
    @Autowired
    private GameRepository gameRepository;

    @Test
    public void shouldAutomaticallyAddCurrentDateAndTimeWhenNewGameIsAddedToDatabase() {
        Game game = new Game();
        game.setName("Mock Game");
        game.setGameState(GameState.REGISTRATION);
        game = this.gameRepository.save(game);

        ZonedDateTime zonedDateTime = ZonedDateTime.now();

        long expectedEpochSecond = zonedDateTime.toEpochSecond();
        long actualEpochSecond = game.getPublished().toEpochSecond();
        double delta = 1.1; // 1-second difference is possible.

        assertEquals(expectedEpochSecond, actualEpochSecond, delta);
    }
}