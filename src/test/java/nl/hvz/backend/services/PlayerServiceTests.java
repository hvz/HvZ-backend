package nl.hvz.backend.services;

import nl.hvz.backend.repositories.PlayerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = {PlayerServiceImpl.class})
class PlayerServiceTests {

    @Autowired
    private PlayerService playerService;

    @MockBean
    private GameService gameService;

    @MockBean
    private PlayerRepository repository;

    @Test
    void shouldGenerateUniqueBiteCodeOf6AlphanumericCharacters() {
        Set<String> biteCodes = new HashSet<>();
        Mockito.when(repository.findBiteCodeByGameId(1L)).thenReturn(biteCodes);
        Mockito.when(gameService.existsById(1L)).thenReturn(true);

        Pattern expected = Pattern.compile("[a-np-z\\d]{4}");

        for (int i = 0; i < 5000; i++) {
            String actual = playerService.generateUniqueBiteCode(1L);
            assertTrue(expected.matcher(actual).matches(), "Bite code does not match pattern.");
            assertFalse(biteCodes.contains(actual), "Bite code is not unique");

            biteCodes.add(actual);
        }
    }

    @Test
    void shouldRespondWithCorrectReasonWhenPlayerCannotBeFound() {
        Long playerId = 1L;
        Mockito.when(repository.findById(playerId)).thenReturn(Optional.empty());

        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> playerService.findById(playerId));

        String expected = "Player with id 1 does not exist.";
        String actual = exception.getReason();

        assertEquals(expected, actual);
    }
}