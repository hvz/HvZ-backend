package nl.hvz.backend.mappers;

import nl.hvz.backend.models.GameState;
import nl.hvz.backend.models.dtos.gamedtos.GameListItemDto;
import nl.hvz.backend.models.dtos.gamedtos.ModifyGameDto;
import nl.hvz.backend.models.dtos.gamedtos.NewGameDto;
import nl.hvz.backend.models.entities.Game;
import static org.junit.jupiter.api.Assertions.*;

import nl.hvz.backend.services.PlayerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = GameMapperImpl.class)
class GameMapperTests {
    @Autowired
    GameMapper gameMapper;
    @MockBean
    PlayerServiceImpl playerService;

    @Test
    public void shouldSetPlayStateToRegistrationWhenConvertingNewGameDto() {
        NewGameDto newGameDto = new NewGameDto();

        GameState expected = GameState.REGISTRATION;
        GameState actual = gameMapper.toNewGameEntity(newGameDto).getGameState();

        assertEquals(expected, actual);
    }
    @Test
    public void shouldMapEnumsToStringsCorrectly() {
        Game game = new Game();
        game.setGameState(GameState.IN_PROGRESS);

        GameListItemDto gameListItemDto = new GameListItemDto();
        gameListItemDto.setGameState("IN_PROGRESS");

        String expected = gameListItemDto.getGameState();
        String actual = gameMapper.toGameListItemDto(game).getGameState();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldMapStringsToEnumsCorrectly() {
        ModifyGameDto modifyGameDto = new ModifyGameDto();
        modifyGameDto.setGameState("IN_PROGRESS");

        Game game = new Game();
        game.setGameState(GameState.IN_PROGRESS);

        GameState expected = game.getGameState();
        GameState actual = assertDoesNotThrow(() -> gameMapper.toUpdateGameEntity(modifyGameDto, 1L, new Game()).getGameState());

        assertEquals(expected, actual);
    }

    @Test
    public void shouldNotModifyDataWhenUsingUpdateMapperWhenDTOValueIsNull() {
        Game game = new Game();
        game.setId(1L);
        game.setName("Foo");

        ModifyGameDto modifyGameDto = new ModifyGameDto();
        modifyGameDto.setGameState("REGISTRATION");

        Game newGame = gameMapper.toUpdateGameEntity(modifyGameDto, 1L, game);

        String expectedName = "Foo";
        String actualName = newGame.getName();
        assertEquals(expectedName, actualName);
    }

    @Test
    public void shouldRespondWithBadRequestIfStringValueIsNotOneOfTheEnumOptions() {
        ModifyGameDto modifyGameDto = new ModifyGameDto();
        modifyGameDto.setGameState("completed");
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class,
                () -> gameMapper.toUpdateGameEntity(modifyGameDto, 1L, new Game()));

        HttpStatus expectedStatus = HttpStatus.BAD_REQUEST;
        HttpStatus actualStatus = responseStatusException.getStatus();

        String expectedReason = "gameState must be 'REGISTRATION', 'IN_PROGRESS', or 'COMPLETED'.";
        String actualReason = responseStatusException.getReason();

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedReason, actualReason);
    }

    @Test
    public void shouldMapZonedDateTimeToISOStandardString() {
        ZonedDateTime zonedDateTime = ZonedDateTime.of(2020,1,1,12,0,0,0, ZoneOffset.ofHours(-2));
        Game game = new Game();
        game.setPublished(zonedDateTime);

        String expected = "2020-01-01T12:00:00-02:00";
        String actual = gameMapper.toGameDetailsDto(game).getPublished();

        assertEquals(expected, actual);
    }
}