package nl.hvz.backend.mappers;

import nl.hvz.backend.models.entities.Game;
import nl.hvz.backend.models.entities.Player;
import nl.hvz.backend.models.entities.User;
import nl.hvz.backend.services.GameServiceImpl;
import static org.junit.jupiter.api.Assertions.*;

import nl.hvz.backend.services.SquadMemberServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ContextConfiguration(classes = PlayerMapperImpl.class)
public class PlayerMapperTests {
    @Autowired
    private PlayerMapper playerMapper;
    @MockBean
    private GameServiceImpl gameService;
    @MockBean
    private SquadMemberServiceImpl squadMemberService;

    @Test
    public void shouldSetDefaultValuesForPatientZeroAndIsHumanCorrectlyWhenConvertingNewPlayerDto() {
        Player player = playerMapper.toNewPlayerEntity(1L, new User(), "123456");

        boolean expectedIsHuman = true;
        boolean actualIsHuman = player.getIsHuman();

        boolean expectedIsPatientZero = false;
        boolean actualIsPatientZero = player.getIsPatientZero();

        assertEquals(expectedIsHuman, actualIsHuman, "isHuman assertion failed");
        assertEquals(expectedIsPatientZero, actualIsPatientZero, "isPatientZero assertion failed");
    }
}