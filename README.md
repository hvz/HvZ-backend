# HvZ backend
This is the backend part of the Human vs. Zombie (HvZ) application:
the final case project of the Noroff full stack Java developer course.

## Installation
To install and run a local development version of the project, you will need git and docker.
To get all the contents of the project, simply clone the git repository.
To launch the local development version, set the working directory to the root directory of the project,
and run `docker-compose up`.

## Environment variables

### `PORT`
The port the backend application will be exposed.
By default, this is `8080`.

### `SPRING_PROFILES_ACTIVE`
The spring profile that should be used. This project makes use of 3 profiles:
- `test`, meant for testing the backend application.
- `dev`, meant to launch a development version of the application on a local machine.
- `prod`, meant for deployment on Heroku.

By default, the `dev` profile is used.

### `JWT_ISSUER_URI`
The URI to the issuer of JWTs.
The project makes use of spring security, which requires an identity provider that issues JWTs.
By default, the URI is https://hvz-case-project.eu.auth0.com/

### `JWT_SET_URI`
The URI to the JWK set of the issuer.
The JWK set is used to verify JWTs of the issuer.
By default, the URI is https://hvz-case-project.eu.auth0.com/.well-known/jwks.json

## Usage

### Testing
When testing, you will need to have gradle and jdk 17 installed on your device.
Alternatively, you can run gradle via docker using `docker run gradle:<tag> <gradle commands>`,
but this is not recommended, because the file output of gradle will end up inside the container.
Instead, make use of an IDE that comes with gradle and facilitates the downloading/using of a jdk.

To run test with the spring profile set to test,
either run in the project root directory
`SPRING_PROFILES_ACTIVE=test gradle test` or `gradle test "-Dspring.profiles.active=test"`.
> **Warning:** the first command does not work in PowerShell.
> Unlike in unix,
> it is necessary to use quotes for the second command due to the special meaning of periods in PowerShell. 

### Local development
Once the local development version is running via `docker-compose up`,
the app can be remotely debugged.
The port that will be used for communication is `5005`.
The easiest way to configure this, is by using IntelliJ IDEA's run/debug configurations.

The postgres database can be accessed as well.
The port exposed to the host device is `2345`.

### Deployment
The project is configured to deploy the application on Heroku.
In order to successfully deploy the application, the following must be done beforehand:
- The postgres addon must be installed.
  Check if the `DATABASE_URL` environment variable is set in the app settings.
- The `SPRING_PROFILES_ACTIVE` environment variable must be set to `prod`.

## Authors
[HvZ group](https://gitlab.com/hvz)

### Members:
- [Kevin Choi](https://gitlab.com/mr-choi)
- [Gea Hendriks](https://gitlab.com/g.m.j.w.hendriks)
- [Beau Casemie](https://gitlab.com/beau-c)
- [Axl Branco Duarte](https://gitlab.com/AxlBrancoDuarte)
