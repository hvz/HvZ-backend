FROM gradle:7.4.2-alpine AS build
COPY src /app/src
COPY build.gradle /app/build.gradle
COPY settings.gradle /app/settings.gradle
WORKDIR /app
RUN gradle bootJar

FROM eclipse-temurin:17-jdk-alpine AS run
COPY --from=build /app/build/libs/*.jar /app/HvZbackend.jar
WORKDIR /app
CMD ["java", "-jar", "HvZbackend.jar"]